.PHONY: all version wheel doc upload clean

all: wheel doc

version:
	bash change_version.sh

wheel:
	cd host && ./setup.py sdist bdist_wheel

doc:
	cd doc && $(MAKE) html

upload: wheel
	cd host && twine upload dist/*

clean:
	- cd doc && $(MAKE) clean
	- rm host/trs_interface.egg-info -r
	- rm host/build -r
	- rm host/dist -r
	- find . -type d -name __pycache__ -exec rm {} -r \;
