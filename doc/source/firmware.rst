Firmware
========

The firmware runs on an Arduino Due board. PlatformIO in CodeOSS (VSCodium, VSCode or equivalent)
with the Arduino framework are the recommended development tools.

Source code is available under the `firmware
<https://gitlab.com/ptapping/trs-interface/-/tree/main/firmware>`__ directory tree.

To flash software to a device (either when building a new device, or updating the firmware of an
exisiting one), open the ``trs-interface.code-workspace`` in VSCode, navigate to
``firmware/src/main.cpp``. Connect the interface via USB to the Arduino Due programming port, then
use the ``PlatformIO: Upload`` function in the bottom toolbar or the PlatformIO sidebar tools.

