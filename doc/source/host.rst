Host Library
============

A python library is used to interface the host computer interfaces to the hardware device.

Source code is available under the `host <https://gitlab.com/ptapping/trs-interface/-/tree/main/host>`__ directory tree.

It can be installed from the `Python Package Index <https://pypi.org/project/trs-interface/>`__ using
``pip install trs-interface`` or similar.

See the :data:`API documentation <trs_interface>` for usage examples.