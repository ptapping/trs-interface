trs\_interface.protocol package
===============================

.. automodule:: trs_interface.protocol
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trs_interface.protocol.decoder
   trs_interface.protocol.encoder
