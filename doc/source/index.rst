Welcome to TRS-Interface's documentation!
===============================================

The TRSInterface is a hardware interface that bridges instruments used for time-resolved spectroscopy
to a data collection computer.
It consists of three main parts:

- :ref:`Hardware`
  - Microcontroller and circuit board design to physically connect instruments to the host computer.
- :ref:`Firmware`
  - Software to run on microcontroller and perform communications with host.
- :ref:`Host Library`
  - Python library to communicate with the hardware.

It is designed to be used as part of a time-resolved spectrometer, in particular the `TRSpectrometer <https://gitlab.com/ptapping/trspectrometer>`__ application.


Hardware designs and software are open source and available at `<https://gitlab.com/ptapping/trs-interface>`__.

User Guide
----------

.. toctree::
   :maxdepth: 2

   hardware
   firmware
   host

API Documentation
-----------------
.. toctree::
   :maxdepth: 5
   :titlesonly:

   api/trs_interface


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
