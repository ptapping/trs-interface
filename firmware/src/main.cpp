/*
 * Copyright 2021 Patrick C. Tapping
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* NOTES:
 *
 * Pin  2: QA in
 * Pin  3: Chopper sync out
 * Pin  4: Laser sync in (joined to pin 30)
 * Pin  5: Camera trigger out
 * Pin 10: (joined to pin 11)
 * Pin 11: (joined to pin 10)
 * Pin 13: QB in
 * Pin 30: Laser sync in (joined to pin 4)
 * Pin  6: Chopper sync in (any available pin, software defined)
 * Pin  8: Delay trig in (any available, software defined)
 *
 * There are inverting buffers between all MCU pins and the physical input/output sockets.
 * This means the digital read/writes will be opposite of what is expected.
 * However, the logic for the "polarity" variables is not inverted, that is, "HIGH" should mean
 * high at the physical socket input/output, which can make the logic somewhat confusing...
 * 
 * The quadrature decoder signals are not inverted, as they pass through a separate non-inverting
 * differential line receiver chip.
 * 
 * Pins 10 and 11 should be connected electrically (chopper pulse counter -> pulse generator)
 * 
 * Pins 4 and 30 should be connected electrically (laser sync in -> chopper pulse counter and camera pulse generator)
 */


#include <Arduino.h>

#include "FastDigital.h"
#include "TRSProtocol.h"
#include "DueFlashStorage.h"


// Pin definitions
// const unsigned char PIN_QA = 2;               // Quadrature phase A input, must be pin 2  (TIOA0)
// const unsigned char PIN_QB = 13;              // Quadrature phase B input, must be pin 13 (TIOB0)
const unsigned char PIN_LASER_SYNC_TC6 = 4;   // Camera trigger generator input, must be pin 4 (TIOB6)
// const unsigned char PIN_CAMERA_TRIG = 5;      // Camera trigger generator output, must be pin 5 (TIOA6)
// const unsigned char PIN_LASER_SYNC_TC8 = 30;  // Chopper pulse counter input, must be pin 30 (TCLK8)
// const unsigned char PIN_CHOPPER_SYNC_OUT = 3; // Chopper pulse generator output, must be pin 3 (TIOA7)
const unsigned char PIN_CHOPPER_SYNC_IN = 6;     // Chopper input state, can be any digital input pin
const unsigned char PIN_DELAY_TRIG_IN = 8;       // Delay trigger input, can be any digital input pin

// Chopper configuration
unsigned long chopper_polarity_in = POLARITY_HIGH;

// Delay stage trigger configuration
unsigned long delay_polarity_in = POLARITY_HIGH;
long quadrature_offset = 0;

// Software (USB) arming flag
volatile bool usb_armed = false;
// Triggered and returning data to host
volatile bool started = false;
// Triggering camera shutter only, no data returned to host
volatile bool triggering = false;

// Time for measuring laser sync frequency
volatile unsigned long sync_time_prev = 0;
volatile unsigned long sync_period = 1;

// Frame count for start(n) fixed acquisition mode
volatile unsigned long frame_count = 0;

// Flash storage for persistent settings
DueFlashStorage storage;

/*
Setting to store:
- laser sync polarity       byte
- chopper syncin polarity   byte
- chopper syncout polarity  byte
- delay trig polarity       byte
- camera trig polarity      byte
- quadrature polarity       byte
- quadrature direction      byte
- chopper delay             uint32
- chopper duration          uint32
- camera delay              uint32
- camera duration           uint32
- chopper divider           byte
*/

// Struct containing settings to read/write from flash storage
struct Configuration {
  uint8_t laser_sync_polarity;
  uint8_t chopper_syncin_polarity;
  uint8_t chopper_syncout_polarity;
  uint8_t delay_trig_polarity;
  uint8_t camera_trig_polarity;
  uint8_t quadrature_polarity;
  uint8_t quadrature_direction;
  uint8_t chopper_divider;
  uint32_t chopper_delay;
  uint32_t chopper_duration;
  uint32_t camera_delay;
  uint32_t camera_duration;
};


// Send a message header out to the computer
void send(short msgid, long msgdata) {
  SerialUSB.write("MSG:");
  SerialUSB.write((byte*)&msgid, sizeof(msgid));
  SerialUSB.write((byte*)&msgdata, sizeof(msgdata));
}


void send_string(short msgid, const char* msgdata) {
  // Header
  long terminator = 0;
  SerialUSB.write("MSG:");
  SerialUSB.write((byte*)&msgid, sizeof(msgid));
  SerialUSB.write((byte*)&terminator, sizeof(terminator));
  // Payload
  SerialUSB.print(msgdata);
  SerialUSB.write((byte*)&terminator, sizeof(terminator));
}


// Sent data terminator and stop sending data
inline void stop() {
  usb_armed = false;
  if (started) {
    REG_TC2_IDR0 = TC_IER_CPAS;    /* disable interrupt on RA compare match */
    REG_TC2_CCR0 = TC_CCR_CLKDIS;  /* disable the clock */
    started = false;
    // Send data terminator
    SerialUSB.write((byte*)&DATA_TERMINATOR, sizeof(DATA_TERMINATOR));
  }
  if (triggering) {
    REG_TC2_CCR0 = TC_CCR_CLKDIS;  /* disable the clock */
    triggering = false;
  }
}


void measure_laser_sync_period() {
  unsigned long t = micros();
  sync_period = t - sync_time_prev;
  sync_time_prev = t;
}


void TC6_Handler() {
  // Write out event to serial port
  // We'll use quadrature position as a signed 31-bit number, with the LSB zero
  // So count will be 2x the measured value, but leaves us one bit for chopper on/off status
  long data = (long)((REG_TC0_CV0 + quadrature_offset) << 1) | (long)(digitalReadFast(PIN_CHOPPER_SYNC_IN) ^ !chopper_polarity_in);
  SerialUSB.write((byte*)&data, sizeof(data));
  if (frame_count > 1) {
    // Keep counting down number of frames requested
    frame_count--;
  } else if (frame_count == 1) {
    // That was the last frame requested, stop now
    stop();
  }
  // Read from status register to reset
  REG_TC2_SR0;
}


// Interrupt handler for incoming trigger signals from the delay stage
void delay_trig_handler() {
  if (usb_armed) {
    // Have received the "armed" signal over the USB connection
    // Enable or disable the camera trigger clock based on delay trigger pin status
    // Note there's an inverting buffer between the MCU pin and the physical socket
    if (digitalReadFast(PIN_DELAY_TRIG_IN) ^ delay_polarity_in) {
      // Delay triggered start signal
      // Send the data message header
      send(MSG_GOT_DATA, DATA_TERMINATOR);
      started = true;
      REG_TC2_IER0 = TC_IER_CPAS;   /* enable interrupt on RA compare match */
      REG_TC2_CCR0 = TC_CCR_CLKEN;  /* enable the clock */
    } else {
      // Delay triggered stop signal
      if (started) {
        // Disable interrupt and clock
        REG_TC2_IDR0 = TC_IER_CPAS;    /* disable interrupt on RA compare match */
        REG_TC2_CCR0 = TC_CCR_CLKDIS;  /* disable the clock */
        usb_armed = false;
        started = false;
        // Send the data message terminator bytes
        SerialUSB.write((byte*)&DATA_TERMINATOR, sizeof(DATA_TERMINATOR));
      }
    }
  }
}


// Write current settings to flash storage
void store_settings() {
  // Build configuration struct
  Configuration config;
  config.laser_sync_polarity = (uint8_t)((REG_TC2_CMR0 & TC_CMR_EEVTEDG_Msk) == TC_CMR_EEVTEDG_FALLING) ? POLARITY_RISING : POLARITY_FALLING;
  config.chopper_syncin_polarity = (uint8_t)chopper_polarity_in;
  config.chopper_syncout_polarity = (uint8_t)((REG_TC2_CMR1 & TC_CMR_ACPA_Msk) == TC_CMR_ACPA_SET) ? POLARITY_LOW : POLARITY_HIGH;
  config.delay_trig_polarity = (uint8_t)delay_polarity_in;
  config.camera_trig_polarity = (uint8_t)((REG_TC2_CMR0 & TC_CMR_ACPA_Msk) == TC_CMR_ACPA_SET) ? POLARITY_LOW : POLARITY_HIGH;
  config.quadrature_polarity = (uint8_t)(REG_TC0_BMR & TC_BMR_INVA) ? POLARITY_LOW : POLARITY_HIGH;
  config.quadrature_direction = (uint8_t)!!(REG_TC0_BMR & TC_BMR_SWAP);
  config.chopper_divider = (uint8_t)REG_TC2_RC2;
  config.chopper_delay = REG_TC2_RA1;
  config.chopper_duration = REG_TC2_RC1 - REG_TC2_RA1;
  config.camera_delay = REG_TC2_RA0;
  config.camera_duration = REG_TC2_RC0 - REG_TC2_RA0;
  // Write to flash
  byte b[sizeof(Configuration)];
  memcpy(b, &config, sizeof(Configuration));
  storage.write(4, b, sizeof(Configuration));
  // Write zero to address 0 to indicate settings exist
  storage.write(0, 0);
}


// Act upon a single message given its id and data fields
void handle_message(short msgid, int msgdata) {
  switch (msgid) {

    case MSG_GET_PING:
      send(MSG_GOT_PING, msgdata);
      break;

    case MSG_GET_VERSION:
      // Version string is a long-form message, four-byte zero terminated string
      send_string(MSG_GOT_VERSION, VERSION);
      break;

    case MSG_STORE_SETTINGS:
      store_settings();
      break;

    case MSG_GET_LASER_SYNC_POLARITY:
      // Just check camera pulse generator input edge polarity
      send(MSG_GOT_LASER_SYNC_POLARITY, ((REG_TC2_CMR0 & TC_CMR_EEVTEDG_Msk) == TC_CMR_EEVTEDG_FALLING) ? POLARITY_RISING : POLARITY_FALLING);
      break;
    case MSG_SET_LASER_SYNC_POLARITY:
      // Adjust camera pulse generator trigger input edge polarity
      REG_TC2_CMR0 = (REG_TC2_CMR0 & ~TC_CMR_EEVTEDG_Msk) | ((msgdata == POLARITY_FALLING) ? TC_CMR_EEVTEDG_RISING : TC_CMR_EEVTEDG_FALLING);
      // Adjust chopper pulse counter clock edge polarity
      if (msgdata == POLARITY_FALLING) {
        REG_TC2_CMR2 &= ~TC_CMR_CLKI;
      } else {
        REG_TC2_CMR2 |= TC_CMR_CLKI;
      }
      break;
    
    case MSG_GET_CHOPPER_SYNCIN_POLARITY:
      send(MSG_GOT_CHOPPER_SYNCIN_POLARITY, chopper_polarity_in);
      break;
    case MSG_SET_CHOPPER_SYNCIN_POLARITY: 
      chopper_polarity_in = (msgdata == POLARITY_LOW) ? POLARITY_LOW : POLARITY_HIGH;
      break;
    
    case MSG_GET_CHOPPER_SYNCOUT_POLARITY:
      // Get polarity from pulse generator CMR register settings
      send(MSG_GOT_CHOPPER_SYNCOUT_POLARITY, ((REG_TC2_CMR1 & TC_CMR_ACPA_Msk) == TC_CMR_ACPA_SET) ? POLARITY_LOW : POLARITY_HIGH);
      break;
    case MSG_SET_CHOPPER_SYNCOUT_POLARITY:
      // Set polarity using pulse generator CMR register settings
      REG_TC2_CMR1 = (REG_TC2_CMR1 & ~(TC_CMR_ACPA_Msk | TC_CMR_ACPC_Msk | TC_CMR_AEEVT_Msk | TC_CMR_ASWTRG_Msk)) |
                      ((msgdata == POLARITY_LOW) ?
                      (TC_CMR_ACPA_SET | TC_CMR_ACPC_CLEAR | TC_CMR_AEEVT_CLEAR | TC_CMR_ASWTRG_CLEAR) :
                      (TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_SET | TC_CMR_AEEVT_SET | TC_CMR_ASWTRG_SET));
      // Software trigger to switch output polarity now
      REG_TC2_CCR1 = TC_CCR_CLKEN | TC_CCR_SWTRG;
      break;
    
    case MSG_GET_DELAY_TRIG_POLARITY:
      send(MSG_GOT_DELAY_TRIG_POLARITY, delay_polarity_in);
      break;
    case MSG_SET_DELAY_TRIG_POLARITY:
      delay_polarity_in = (msgdata == POLARITY_FALLING) ? POLARITY_FALLING : POLARITY_RISING;
      break;

    case MSG_GET_CAMERA_TRIG_POLARITY:
      // Get polarity from pulse generator CMR register settings
      send(MSG_GOT_CAMERA_TRIG_POLARITY, ((REG_TC2_CMR0 & TC_CMR_ACPA_Msk) == TC_CMR_ACPA_SET) ? POLARITY_LOW : POLARITY_HIGH);
      break;
    case MSG_SET_CAMERA_TRIG_POLARITY:
      // Set polarity using pulse generator CMR register settings
      REG_TC2_CMR0 = (REG_TC2_CMR0 & ~(TC_CMR_ACPA_Msk | TC_CMR_ACPC_Msk | TC_CMR_AEEVT_Msk | TC_CMR_ASWTRG_Msk)) |
                      ((msgdata == POLARITY_LOW) ? 
                      (TC_CMR_ACPA_SET | TC_CMR_ACPC_CLEAR | TC_CMR_AEEVT_CLEAR | TC_CMR_ASWTRG_CLEAR) :
                      (TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_SET | TC_CMR_AEEVT_SET | TC_CMR_ASWTRG_SET));
      // Software trigger to switch output polarity now
      REG_TC2_CCR0 = TC_CCR_CLKDIS | TC_CCR_SWTRG;
      break;
    
    case MSG_GET_QUADRATURE_POLARITY:
      send(MSG_GOT_QUADRATURE_POLARITY, (REG_TC0_BMR & TC_BMR_INVA) ? POLARITY_LOW : POLARITY_HIGH);
      break;
    case MSG_SET_QUADRATURE_POLARITY:
      if (msgdata == POLARITY_LOW) {
        REG_TC0_BMR = REG_TC0_BMR | TC_BMR_INVA;
        REG_TC0_BMR = REG_TC0_BMR | TC_BMR_INVB;
      } else {
        REG_TC0_BMR = REG_TC0_BMR & ~TC_BMR_INVA;
        REG_TC0_BMR = REG_TC0_BMR & ~TC_BMR_INVB;
      }
      break;

    case MSG_GET_QUADRATURE_DIRECTION:
      send(MSG_GOT_QUADRATURE_DIRECTION, !!(REG_TC0_BMR & TC_BMR_SWAP));
      break;
    case MSG_SET_QUADRATURE_DIRECTION:
      if (msgdata == DIRECTION_REVERSE) {
        REG_TC0_BMR = REG_TC0_BMR | TC_BMR_SWAP;
      } else {
        REG_TC0_BMR = REG_TC0_BMR & ~TC_BMR_SWAP;
      }
      break;
    
    case MSG_GET_CHOPPER_SYNC_DELAY:
      // Get value from pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      send(MSG_GOT_CHOPPER_SYNC_DELAY, REG_TC2_RA1);
      break;
    case MSG_SET_CHOPPER_SYNC_DELAY:
      // Update RC to maintain same pulse duration
      REG_TC2_RC1 = (unsigned int)msgdata + REG_TC2_RC1 - REG_TC2_RA1;
      // Set value in pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      REG_TC2_RA1 = max(1, (unsigned int)msgdata);
      break;
    
    case MSG_GET_CHOPPER_SYNC_DURATION:
      // Get value from pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      send(MSG_GOT_CHOPPER_SYNC_DURATION, REG_TC2_RC1 - REG_TC2_RA1);
      break;
    case MSG_SET_CHOPPER_SYNC_DURATION:
      // Set value in pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      REG_TC2_RC1 = REG_TC2_RA1 + max(1, (unsigned int)msgdata);
      break;

    case MSG_GET_CAMERA_SYNC_DELAY:
      // Get value from pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      send(MSG_GOT_CAMERA_SYNC_DELAY, REG_TC2_RA0);
      break;
    case MSG_SET_CAMERA_SYNC_DELAY:
      // Update RC to maintain same pulse duration
      REG_TC2_RC0 = (unsigned int)msgdata + REG_TC2_RC0 - REG_TC2_RA0;
      // Set value in pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      REG_TC2_RA0 = max(1, (unsigned int)msgdata);
      
      break;
    
    case MSG_GET_CAMERA_SYNC_DURATION:
      // Get value from pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      send(MSG_GOT_CAMERA_SYNC_DURATION, REG_TC2_RC0 - REG_TC2_RA0);
      break;
    case MSG_SET_CAMERA_SYNC_DURATION:
      // Set value in pulse generator register, values in 42 MHz clock ticks (~23.8 ns)
      REG_TC2_RC0 = REG_TC2_RA0 + max(1, (unsigned int)msgdata);
      break;
    
    case MSG_GET_CHOPPER_DIVIDER:
      // Get value from pulse counter register
      send(MSG_GOT_CHOPPER_DIVIDER, REG_TC2_RC2);
      break;
    case MSG_SET_CHOPPER_DIVIDER:
      // Set value in pulse counter register
      REG_TC2_RC2 = max(2, min(100, msgdata));
      break;

    case MSG_GET_QUADRATURE_VALUE:
      send(MSG_GOT_QUADRATURE_VALUE, REG_TC0_CV0 + quadrature_offset);
      break;
    case MSG_SET_QUADRATURE_VALUE:
      quadrature_offset = msgdata - REG_TC0_CV0;
      break;

    case MSG_GET_LASER_SYNC_PERIOD:
      // Check for > 1s since last pulse, indicative of stopped laser
      send(MSG_GOT_LASER_SYNC_PERIOD, (micros() - sync_time_prev > 1000000) ? 0 : sync_period);
      break;

    case MSG_ARM:
      // frame count of zero means continuous, until external or manual stop
      frame_count = 0;
      usb_armed = bool(msgdata == 1);
      break;

    case MSG_START:
      // frame count of zero means continuous until manual stop
      frame_count = msgdata;
      // Enable the interrupt and enable the clock
      send(MSG_GOT_DATA, DATA_TERMINATOR);
      started = true;
      REG_TC2_IER0 = TC_IER_CPAS;   /* interrupt on RA compare match */
      REG_TC2_CCR0 = TC_CCR_CLKEN;
      break;
    
    case MSG_TRIGGER:
      // Disable the interrupt, start the clock
      triggering = true;
      REG_TC2_IDR0 = TC_IER_CPAS;   /* no interrupt on RA compare match */
      REG_TC2_CCR0 = TC_CCR_CLKEN;
      break;

    case MSG_STOP:
      // Triggering mode doesn't get stopped automatically on incoming messages,
      // but armed and started modes do (as they have to send info back to the host)
      if (triggering) {
        stop();
      }
      break;
    
    default:
      send(MSG_GOT_UNKNOWN_MSG, msgid);
      break;
  }      
}


// Check for incoming messages and handle them
void handle_messages() {
  while (SerialUSB.available() >= 10) {
    // Enough data in buffer to attempt to decode a message
    // Look for magic string
    int i = 0;
    for (; i < 4; i++) {
      if (SerialUSB.peek() != "MSG:"[i]) break;
      SerialUSB.read();
    }
    if (i < 4) continue;
    // Magic string found
    // Disarm or stop on any incoming message
    if (usb_armed | started) {
      stop();
    }
    // Read ID and data fields
    short msgid = 0;
    SerialUSB.readBytes((byte*)&msgid, sizeof(msgid));
    int msgdata = 0;
    SerialUSB.readBytes((byte*)&msgdata, sizeof(msgdata));
    handle_message(msgid, msgdata);
  }
}


void setup() {
  // Initialise the native USB serial connection
  SerialUSB.begin(0);

  // Configure IO pins (ones not driven by hardware timer/counters)
  pinMode(PIN_CHOPPER_SYNC_IN, INPUT);
  pinMode(PIN_DELAY_TRIG_IN, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_DELAY_TRIG_IN), delay_trig_handler, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_LASER_SYNC_TC6), measure_laser_sync_period, FALLING);

  pmc_set_writeprotect(false);    // Not needed, but might as well to be safe

  // Hardware quadrature decoder setup, uses TC0 channel 1 (and maybe channel 2, although we don't use the index signal)
  // QA input on TIOA (pin 2), QB input on TIOB (pin 13)
  pmc_enable_periph_clk(ID_TC0);     /* enable peripheral on power management controller, TC0 means TC0[0] */
  REG_TC0_CMR0 = TC_CMR_TCCLKS_XC0;  /* capture mode enabled, using external clock 0 as source */
  REG_TC0_BMR = TC_BMR_QDEN |        /* activate quadrature encoder */
                TC_BMR_POSEN |       /* position measurement mode */
                TC_BMR_EDGPHA;       /* detecting edges on QA (no filtering) */
  REG_TC0_CCR0 = TC_CCR_CLKEN |      /* enable clock */
                 TC_CCR_SWTRG;       /* and software trigger start */

  // Camera sync pulse generator, uses TC6 (TC2 channel 0)
  // Laser sync input to external trigger TIOB (pin 4), output on TIOA (pin 5)
  pmc_enable_periph_clk(ID_TC6);                 /* enable peripheral on power management controller, TC6 means TC2[0] */
  NVIC_EnableIRQ(TC6_IRQn);                      /* enable IRQ on the interrupt controller, TC6 means TC2[0] */
  REG_PIOC_ABSR |= PIO_ABSR_P25 | PIO_ABSR_P26;  /* use peripheral "B" function, enables TIOA6 and TIOB6 */
  REG_PIOC_PDR |= PIO_PDR_P25 | PIO_PDR_P26;     /* and disable GPIO on those pins */ 
  REG_TC2_CMR0 = 
    TC_CMR_TCCLKS_TIMER_CLOCK1 |  /* use the MCLK/2 clock source */
    TC_CMR_CPCSTOP |              /* stop clock on RC compare match */ 
    TC_CMR_EEVTEDG_FALLING |      /* external event on falling edge */
    TC_CMR_EEVT_TIOB |            /* external event on TIOB input pin */
    TC_CMR_ENETRG |               /* reset and start clock on external event */
    TC_CMR_WAVSEL_UP_RC |         /* counting up with trigger on RC compare match */
    TC_CMR_WAVE |                 /* waveform mode enabled */
    TC_CMR_ACPA_CLEAR |           /* clear TIOA pin on RA compare match */
    TC_CMR_ACPC_SET |             /* set TIOA pin on RC compare match */
    TC_CMR_AEEVT_SET |            /* set TIOA pin on external event */
    TC_CMR_ASWTRG_SET;            /* set TIOA pin on software trigger */
  REG_TC2_RA0 = 1;                /* RA value to compare to, in clock ticks */
  REG_TC2_RC0 = 2;                /* RC value to compare to, in clock ticks */
  REG_TC2_IDR0 = 0xFFFFFFFF;      /* disable all interrupts */
  REG_TC2_CCR0 = TC_CCR_CLKDIS |  /* disable the clock */
                 TC_CCR_SWTRG;    /* software trigger to set initial state */

  // Chopper pulse counter, uses TC8 (TC2 channel 2)
  // Laser sync input to external clock TCLK8 (pin 30), output on TIOA (pin 11) to trigger next clock (TC7 TIOB, pin 10) 
  pmc_enable_periph_clk(ID_TC8);               /* enable peripheral on power management controller, TC8 means TC2[2] */
  REG_PIOD_ABSR |= PIO_ABSR_P7 | PIO_ABSR_P9;  /* use peripheral "B" function, enables TIOA8 and TCLK8 */
  REG_PIOD_PDR |= PIO_PDR_P7 | PIO_PDR_P9;     /* and disable GPIO on those pins */ 
  REG_TC2_CMR2 = 
    TC_CMR_TCCLKS_XC2 |   /* select external clock 2 */
    TC_CMR_CLKI |         /* increment on falling edge */
    TC_CMR_WAVSEL_UP_RC | /* count up with trigger on RC compare match */
    TC_CMR_WAVE |         /* waveform mode */
    TC_CMR_ACPA_CLEAR |   /* clear TIOA on RA compare match */
    TC_CMR_ACPC_SET;      /* set TIOA on RC compare match */
  REG_TC2_RA2 = 1;               /* RA value to compare to */
  REG_TC2_RC2 = 2;               /* RC value to compare to */
  REG_TC2_CCR2 = TC_CCR_CLKEN |  /* enable clock */
                 TC_CCR_SWTRG;   /* and software trigger start */

  // Chopper pulse generator, uses TC7 (TC2 channel 1)
  // TC8 TIOA output (pin 11) connected to TC7 trigger TIOB (pin 10), output on TIOA (pin 3)
  pmc_enable_periph_clk(ID_TC7);                 /* enable peripheral on power management controller, TC7 means TC2[1] */
  REG_PIOC_ABSR |= PIO_ABSR_P28 | PIO_ABSR_P29;  /* use peripheral "B" function, enables TIOA7 and TIOB7 */
  REG_PIOC_PDR |= PIO_PDR_P28 | PIO_PDR_P29;     /* and disable GPIO on those pins */ 
  REG_TC2_CMR1 = 
    TC_CMR_TCCLKS_TIMER_CLOCK1 |  /* use the MCLK/2 clock source */
    TC_CMR_CPCSTOP |              /* stop clock on RC compare match */ 
    TC_CMR_EEVTEDG_RISING |       /* external event on rising edge */
    TC_CMR_EEVT_TIOB |            /* external event on TIOB input pin */
    TC_CMR_ENETRG |               /* reset and start clock on external event */
    TC_CMR_WAVSEL_UP_RC |         /* counting up with trigger on RC compare match */
    TC_CMR_WAVE |                 /* waveform mode enabled */
    TC_CMR_ACPA_CLEAR |           /* clear TIOA pin on RA compare match */
    TC_CMR_ACPC_SET |             /* set TIOA pin on RC compare match */
    TC_CMR_AEEVT_SET |            /* set TIOA pin on external event */
    TC_CMR_ASWTRG_SET;            /* set TIOA pin on software trigger */
  REG_TC2_RA1 = 1;                /* RA value to compare to */
  REG_TC2_RC1 = 42;               /* RC value to compare to */
  REG_TC2_CCR1 = TC_CCR_CLKEN |   /* enable the clock */
                 TC_CCR_SWTRG;    /* software trigger to set initial state */  

  // Check if saved settings exist in flash storage
  if (storage.read(0) == 0x00) {
    // Freshly erased flash bytes should read 255, zero in first byte means settings exist
    // Read into a struct starting from byte 4
    byte* b = storage.readAddress(4);
    Configuration config;
    memcpy(&config, b, sizeof(Configuration));
    // Apply each setting from the struct
    handle_message(MSG_SET_LASER_SYNC_POLARITY, config.laser_sync_polarity);
    handle_message(MSG_SET_CHOPPER_SYNCIN_POLARITY, config.chopper_syncin_polarity);
    handle_message(MSG_SET_CHOPPER_SYNCOUT_POLARITY, config.chopper_syncout_polarity);
    handle_message(MSG_SET_DELAY_TRIG_POLARITY, config.delay_trig_polarity);
    handle_message(MSG_SET_CAMERA_TRIG_POLARITY, config.camera_trig_polarity);
    handle_message(MSG_SET_QUADRATURE_POLARITY, config.quadrature_polarity);
    handle_message(MSG_SET_QUADRATURE_DIRECTION, config.quadrature_direction);
    handle_message(MSG_SET_CHOPPER_DIVIDER, config.chopper_divider);
    handle_message(MSG_SET_CHOPPER_SYNC_DELAY, config.chopper_delay);
    handle_message(MSG_SET_CHOPPER_SYNC_DURATION, config.chopper_duration);
    handle_message(MSG_SET_CAMERA_SYNC_DELAY, config.camera_delay);
    handle_message(MSG_SET_CAMERA_SYNC_DURATION, config.camera_duration);
  }
}


void loop() {
  handle_messages();
}
