#ifndef TRSPROTOCOL_H
#define TRSPROTOCOL_H

const char VERSION[] = "1.4.3";

// MSB indicates extended message type, where data field encodes payload size
const short LONG_FORM = 0x8000;

// Terminator value to indicate end of extended data payload
const long DATA_TERMINATOR = 0x8880777f;

// System messages
const short MSG_GET_PING = 0x0000;
const short MSG_GOT_PING = 0x0001;

const short MSG_GOT_UNKNOWN_MSG = 0x0011;

const short MSG_GET_VERSION = 0x0020;
const short MSG_GOT_VERSION = 0x0021 | LONG_FORM;

const short MSG_STORE_SETTINGS = 0x0802;

// Configuration messages
const short MSG_GET_LASER_SYNC_POLARITY = 0x1000;
const short MSG_GOT_LASER_SYNC_POLARITY = 0x1001;
const short MSG_SET_LASER_SYNC_POLARITY = 0x1002;

const short MSG_GET_CHOPPER_SYNCIN_POLARITY = 0x1010;
const short MSG_GOT_CHOPPER_SYNCIN_POLARITY = 0x1011;
const short MSG_SET_CHOPPER_SYNCIN_POLARITY = 0x1012;

const short MSG_GET_CHOPPER_SYNCOUT_POLARITY = 0x1020;
const short MSG_GOT_CHOPPER_SYNCOUT_POLARITY = 0x1021;
const short MSG_SET_CHOPPER_SYNCOUT_POLARITY = 0x1022;

const short MSG_GET_DELAY_TRIG_POLARITY = 0x1030;
const short MSG_GOT_DELAY_TRIG_POLARITY = 0x1031;
const short MSG_SET_DELAY_TRIG_POLARITY = 0x1032;

const short MSG_GET_CAMERA_TRIG_POLARITY = 0x1050;
const short MSG_GOT_CAMERA_TRIG_POLARITY = 0x1051;
const short MSG_SET_CAMERA_TRIG_POLARITY = 0x1052;

const short MSG_GET_QUADRATURE_POLARITY = 0x1060;
const short MSG_GOT_QUADRATURE_POLARITY = 0x1061;
const short MSG_SET_QUADRATURE_POLARITY = 0x1062;

const short MSG_GET_QUADRATURE_DIRECTION = 0x1070;
const short MSG_GOT_QUADRATURE_DIRECTION = 0x1071;
const short MSG_SET_QUADRATURE_DIRECTION = 0x1072;

const short MSG_GET_CHOPPER_SYNC_DELAY = 0x1100;
const short MSG_GOT_CHOPPER_SYNC_DELAY = 0x1101;
const short MSG_SET_CHOPPER_SYNC_DELAY = 0x1102;

const short MSG_GET_CHOPPER_SYNC_DURATION = 0x1110;
const short MSG_GOT_CHOPPER_SYNC_DURATION = 0x1111;
const short MSG_SET_CHOPPER_SYNC_DURATION = 0x1112;

const short MSG_GET_CAMERA_SYNC_DELAY = 0x1120;
const short MSG_GOT_CAMERA_SYNC_DELAY = 0x1121;
const short MSG_SET_CAMERA_SYNC_DELAY = 0x1122;

const short MSG_GET_CAMERA_SYNC_DURATION = 0x1130;
const short MSG_GOT_CAMERA_SYNC_DURATION = 0x1131;
const short MSG_SET_CAMERA_SYNC_DURATION = 0x1132;

const short MSG_GET_CHOPPER_DIVIDER = 0x1200;
const short MSG_GOT_CHOPPER_DIVIDER = 0x1201;
const short MSG_SET_CHOPPER_DIVIDER = 0x1202;

const short MSG_GET_QUADRATURE_VALUE = 0x1210;
const short MSG_GOT_QUADRATURE_VALUE = 0x1211;
const short MSG_SET_QUADRATURE_VALUE = 0x1212;

// Command messages
const short MSG_TRIGGER = 0x2004;
const short MSG_ARM = 0x2008;
const short MSG_START = 0x2018;
const short MSG_STOP = 0x2019;

// Data messages
const short MSG_GOT_DATA = 0x4001 | LONG_FORM;

// Laser sync period measurement
const short MSG_GET_LASER_SYNC_PERIOD = 0x4100;
const short MSG_GOT_LASER_SYNC_PERIOD = 0x4101;

// Signal polarity definitions
const unsigned long POLARITY_LOW = 0;
const unsigned long POLARITY_HIGH = 1;
const unsigned long POLARITY_FALLING = 2;
const unsigned long POLARITY_RISING = 3;

// Quadrature direction definitions
const unsigned long DIRECTION_FORWARD = 0;
const unsigned long DIRECTION_REVERSE = 1;

#endif // TRSPROTOCOL_H