#include <Arduino.h>

#ifndef FASTDIGITAL_H
#define FASTDIGITAL_H

// Fast digital read and write functions which more directly poke at the registers

inline int digitalReadFast(int pin) {
  return !!(g_APinDescription[pin].pPort -> PIO_PDSR & g_APinDescription[pin].ulPin);
}

inline void digitalWriteFast(int pin, boolean val) {
  if (val) {
    g_APinDescription[pin].pPort -> PIO_SODR = g_APinDescription[pin].ulPin;
  } else {
    g_APinDescription[pin].pPort -> PIO_CODR = g_APinDescription[pin].ulPin;
  }
}

#endif