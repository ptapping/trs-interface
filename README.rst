TRS-Interface
===================

Hardware design, firmware, and host interface software for the TRSpectrometer interface device.


Support
-------

Documentation can be found online at `<https://trs-interface.readthedocs.io/en/latest/>`__.

Source code available at `<https://gitlab.com/ptapping/trs-interface>`__.

Bug reports, feature requests and suggestions can be submitted to the `issue tracker <https://gitlab.com/ptapping/trs-interface/-/issues>`__.


License
-------

All original code is free and open source, licensed under the GNU Public License.
See the `LICENSE <https://gitlab.com/ptapping/trs-interface/-/blob/main/LICENSE>`__ for details.

The hardware design files are licensed under the TAPR Open Hardware License.
See the `LICENSE <https://gitlab.com/ptapping/trs-interface/-/blob/main/hardware/LICENSE>`__ for details.

Documentation is licensed under the Creative Commons Attribution-ShareAlike 4.0 International (`CC BY-SA 4.0 <http://creativecommons.org/licenses/by-sa/4.0/>`__).
