#!/usr/env/python3

"""
Generates snippets of python code for message type enum, encoder and decoder
by parsing the firmware's protocol definition header file.
These code snippets can then be copy/pasted into the appropriate python files
and edited as needed.

Only needed to ease development when the protocol definition changes.
"""

import os
import re

this_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
with open(os.path.join(this_dir, "../../../firmware/include/TRSProtocol.h"), "r") as f:
    msgs = re.findall("^const short MSG_(.+?) *= *(.+?);", f.read(), re.MULTILINE)

pycode = "# Autogenerated from TRSProtocol.h\n\n"

pycode += "#"*80 + "\n"
pycode += "# ID enum values\n"
pycode += "#"*80 + "\n"
for m in msgs:
    pycode += f"    {m[0]} = {m[1]}\n"
pycode += "\n"
    
pycode += "#"*80 + "\n"
pycode += "# decoder.py functions to decode message byte array data\n"
pycode += "#"*80 + "\n"
for m in msgs:
    pycode += f"@_decoder(ID.{m[0]})  # id={m[1]}\n"
    pycode += f"def {m[0].lower()}(data_raw: bytes) -> Dict[str, Any]:\n"
#    pycode +=  "    # TODO: Autogenerated, edit/add parameters as required.\n"
    pycode +=  "    return {}\n\n"
    
pycode += "#"*80 + "\n"
pycode += "# encoder.py functions to encode message data to byte array\n"
pycode += "#"*80 + "\n"
for m in msgs:
    paramname=m[0].lower().rsplit('_')[-1]
    pycode += f"def {m[0].lower()}({paramname}:int=0) -> bytes:\n"
#    pycode +=  "    # TODO: Autogenerated, edit/remove parameters as required.\n"
    pycode += f"    return pack(ID.{m[0]}, data={paramname})\n\n"

pycode += "#"*80 + "\n"
pycode += "# TRSInterface status properties\n"
pycode += "#"*80 + "\n"
pycode += "self.status = {\n"
for m in msgs:
    if m[0][:3] == "GET":
        pycode += f"   \"{m[0].lower().split('_', maxsplit=1)[1]}\" : None,\n"
pycode += "}\n"



with open(os.path.join(this_dir, "protocol_pycode.txt"), "w") as f:
    f.write(pycode)